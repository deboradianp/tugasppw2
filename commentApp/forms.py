from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi status',
    }
    description_attrs ={
    	'type':'text',
    	'rows':4,
    	'class':'status-form-textarea',
    	'placeholder':"What's on your mind?"
    }

    status = forms.CharField(widget=forms.Textarea(attrs=description_attrs), required=True, label='')


class Comment_Form(forms.Form):
    error_messages ={
        'required':'Tolong isi comment'
    }
    description_attrs ={
        'type':'text',
        'rows':1,
        'class':'comment-form-textarea',
        'placeholder':'Please give a comment'
    }
    nama_attrs={
        'class':'comment-form-textarea',
        'placeholder':'Put your name here'
    }

    nama = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=nama_attrs))
    comment = forms.CharField(widget=forms.Textarea(attrs=description_attrs), required=True, max_length = 160, label='')
