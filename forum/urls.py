from django.conf.urls import url
from .views import index, add_message, dashboard
from .custom_auth import auth_login, auth_logout

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^add_message/(?P<id>.*)/$', add_message, name='add_message'),
	url(r'^dashboard/$', dashboard, name='dashboard'),
	url(r'^custom_auth/login/$', auth_login, name='auth_login'),
	url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),
]
