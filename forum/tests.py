from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class ForumUnitTest(TestCase):
	def test_profil_url_is_exist(self):
		response = self.client.get('/forum/')
		self.assertEqual(response.status_code, 200)

	def test_profil_using_index_func(self):
		found = resolve('/forum/')
		self.assertEqual(found.func, index)
