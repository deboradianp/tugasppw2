from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
import requests

# Create your views here.
def index(request):
    return render(request, 'login/login.html')

# def auth(request):
#     url = "https://www.linkedin.com/oauth/v2/authorization"
#     params = {
#         "response_type" : "code",
#         "client_id" : "81fruveyl5xurr",
#         "redirect_uri" : "https://localhost:8000/login/callback",
#         "state" : "DCEeFWf45A53sdfKef424"
#     }

#     print(requests.get(url, params=params).text)
#     return render(request, 'login/login.html')

def callback(request):
    print("masuk sini")
    if request.method == "GET":
        code = request.GET.get('code', '')

        url = "https://www.linkedin.com/oauth/v2/accessToken"

        params = {
            "grant_type" : "authorization_code",
            "code" : str(code),
            "redirect_uri" : "http://localhost:8000/login/callback&state=987654321",
            "client_id" : "812u1c12sam3fr",
            "client_secret" : "sT6F3AARbGdSpSOV"
        }

        accessToken = requests.get(url, params=params).json().get('access_token')

        params = {
            "oauth2_access_token" : accessToken,
            "format" : "json",
            "is-company-admin" : "true"
        }
        url2 = "https://api.linkedin.com/v1/companies"

        companyId = requests.get(url2, params=params).json()['values'][0]['id']

        params = {
            "oauth2_access_token" : accessToken,
            "format" : "json"
        }
        url3 = "https://api.linkedin.com/v1/companies/13602577"+":(name,company-type,website-url,logo-url,specialties)"

        jr = requests.get(url3, params=params).json()

        tipePerusahaan = jr['companyType']['name']
        logoUrl = jr["logoUrl"]
        name = jr["name"]
        specialties = jr["specialties"]["values"]
        print(type(specialties))
        websiteUrl = jr["websiteUrl"]

        response = {}

        response["name"] = name
        response["logoUrl"] = logoUrl
        response["tipePerusahaan"] = tipePerusahaan
        response["specialties"] = specialties
        response["websiteUrl"] = websiteUrl

        return render(request, "login/company.html", response)


    return HttpResponseRedirect(reverse('login:index'))
