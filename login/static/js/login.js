window.linkedinInit = () = => {
  IN.init({
    api_key: 81lbbthhfds6i3
    authorize: true
    onLoad: onLinkedInLoad
  });

  IN.getLoginStatus(function(response){
    if(IN.User.isAuthorized() == true){
      render(true);
    }
    else if(IN.User.isAuthorized() == false){
      render(false);
    }
    else{
      render(false)
    }
  });

}
// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
  IN.Event.on(IN, "auth", getProfileData);
}

// Handle the successful return from the API call
function onSuccess(data) {
  console.log(data);
}

// Handle an error response from the API call
function onError(error) {
  console.log(error);
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
  IN.API.Raw("/people/~").result(onSuccess).error(onError);
}
