from django.conf.urls import url
from .views import index
from forum.views import dashboard

    #url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^forum/',dashboard, name='forum'),
]
