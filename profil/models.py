from django.db import models

# Create your models here.
class Perusahaan(models.Model):
	tipe_perusahaan = models.CharField(max_length=500)
	web_perusahaan = models.CharField(max_length=500)
	spesialitas = models.CharField(max_length=500)