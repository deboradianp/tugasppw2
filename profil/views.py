from django.shortcuts import render
from .models import Perusahaan

# Create your views here.
response = {} 
def index(request):
	response['author'] = "Buka Hati"
	response['tipe'] = "Non profit"
	response['web'] = "htttp://bukahati.com"
	response['spesialitas']="Zakat Berbasis Web"
	html = 'profil/profil.html'
	return render(request, html, response)
